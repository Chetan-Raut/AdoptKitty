import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SettingsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Text(
              'Settings Page',
              style: TextStyle(fontSize: 24),
            ),
          ),
          SizedBox(height: 20),
          Center(
            child: Text(
              'Made with 💙 Chetan-Raut',
              style: TextStyle(fontSize: 16),
            ),
          ),
          SizedBox(height: 8),
          Center(
            child: InkWell(
              child: Text(
                'LinkedIn',
                style: TextStyle(
                  fontSize: 16,
                  color: Colors.blue,
                  decoration: TextDecoration.underline,
                ),
              ),
              onTap: () {
                _launchLinkedInURL();
              },
            ),
          ),
        ],
      ),
    );
  }

  _launchLinkedInURL() async {
    const url = 'https://www.linkedin.com/in/chetan-raut2003/';

    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
