import 'package:flutter/material.dart';
import 'settings.dart';

void main() {

  runApp(CatAdoptionApp());

}

class CatAdoptionApp extends StatelessWidget {

  @override

  Widget build(BuildContext context) {

    return MaterialApp(

      title: 'AdoptKitty',

      theme: ThemeData(

        primarySwatch: Colors.blue,

        colorScheme: ColorScheme.fromSwatch(

          primarySwatch: Colors.blue,

          accentColor: Colors.pinkAccent,

        ),

      ),

      home: CatAdoptionPage(),

    );

  }

}

class CatAdoptionPage extends StatelessWidget {

  @override

  Widget build(BuildContext context) {

    return Scaffold(

      appBar: AppBar(

        title: Text('AdoptKitty 🐈'),

      ),

      body: SingleChildScrollView(

        child: Column(

          crossAxisAlignment: CrossAxisAlignment.stretch,

          children: <Widget>[

            Image.asset(

              'assets/Cat1.png',

              height: 200,

              fit: BoxFit.cover,

            ),

            SizedBox(height: 10),

            Image.asset(

              'assets/Cat2.png',

              height: 200,

              fit: BoxFit.cover,

            ),

            SizedBox(height: 10),

            Image.asset(

              'assets/Cat3.png',

              height: 200,

              fit: BoxFit.cover,

            ),

            // Add more Image.asset widgets for additional images

          ],

        ),

      ),

      bottomNavigationBar: BottomNavigationBar(

        items: const <BottomNavigationBarItem>[

          BottomNavigationBarItem(

            icon: Icon(Icons.home),

            label: 'Home',

          ),

          BottomNavigationBarItem(

            icon: Icon(Icons.favorite),

            label: 'Favorites',

          ),

          BottomNavigationBarItem(

            icon: Icon(Icons.settings),

            label: 'Settings',

          ),

        ],

        selectedItemColor: Colors.blue,

      ),

      floatingActionButton: FloatingActionButton(

        onPressed: () {

          // Add functionality to navigate to adoption form

        },

        child: Icon(Icons.add),

        backgroundColor: Colors.blue,

      ),

    );

  }

}

class CatCard extends StatelessWidget {

  final String name;

  final String age;

  final String imageUrl;

  const CatCard({

    Key? key,

    required this.name,

    required this.age,

    required this.imageUrl,

  }) : super(key: key);

  @override

  Widget build(BuildContext context) {

    return Card(

      margin: EdgeInsets.all(10),

      child: Column(

        crossAxisAlignment: CrossAxisAlignment.start,

        children: <Widget>[

          Image.network(

            imageUrl,

            width: double.infinity,

            height: 200,

            fit: BoxFit.cover,

          ),

          Padding(

            padding: EdgeInsets.all(10),

            child: Column(

              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[

                Text(

                  name,

                  style: TextStyle(

                    fontSize: 20,

                    fontWeight: FontWeight.bold,

                  ),

                ),

                SizedBox(height: 5),

                Text(

                  'Age: $age',

                  style: TextStyle(

                    fontSize: 16,

                  ),

                ),

                SizedBox(height: 10),

                Row(

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                  children: <Widget>[

                    ElevatedButton(

                      onPressed: () {

                        // Add functionality to handle adoption

                      },

                      child: Text('Adopt'),

                    ),

                    IconButton(

                      onPressed: () {

                        // Add functionality to share cat details

                      },

                      icon: Icon(Icons.share),

                      color: Colors.blue,

                    ),

                  ],

                ),

              ],

            ),

          ),

        ],

      ),

    );

  }

}   